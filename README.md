# gitlab2discord-hook

Just drop the project somewhere and run `dub` to compile and run it.

This will start a webserver on port 2116 by default, use nginx reverse proxy to make it go through a proper URL.

To use, create a WebHook in Discord, then point Gitlab at

```
https://my-domain.example.com/?hook=https://discordapp.com/api/webhooks/<discord webhook>
```

To test in gitlab, append `&debug` to the URL which will make it wait for a response by discord.
