import vibe.vibe;

import core.time;

import std.uuid;
import std.uri : encodeComponent;

string[] whitelistURLs = ["https://discordapp.com/api/webhooks/"];

void main()
{
	auto settings = new HTTPServerSettings;
	settings.port = 2116;
	settings.bindAddresses = ["::1", "127.0.0.1"];
	listenHTTP(settings, &translate);

	runApplication();
}

void translate(HTTPServerRequest req, HTTPServerResponse res)
{
	if (req.method == HTTPMethod.GET)
	{
		res.writeBody("Usage:\nPOST /?hook=<your discord webhook>");
	}
	else if (req.method == HTTPMethod.POST)
	{
		string url = req.query.get("hook", "");
		bool ok;
		foreach (listed; whitelistURLs)
		{
			if (url.startsWith(listed))
			{
				ok = true;
				break;
			}
		}
		if (!ok)
		{
			res.writeBody("Hook not whitelisted", 400);
			return;
		}

		string event = req.headers.get("X-Gitlab-Event", "");
		if (!event.length)
		{
			res.writeBody("no X-Gitlab-Event header was sent", 400);
			return;
		}

		auto json = req.bodyReader.readAllUTF8;

		if ("debug" in req.query)
		{
			try
			{
				string output = sendTranslated(event, json, url);
				if (output == "success")
					res.writeBody(output, 200);
				else
					res.writeBody(output, 400);
			}
			catch (Exception e)
			{
				res.writeBody(e.msg, 500);
			}
		}
		else
		{
			res.writeBody("ok", 200);
			runTask(&sendTranslated, event, json, url);
		}
	}
}

T shorten(T)(T range, int length)
{
	if (range.length > length)
		return range[0 .. length];
	else
		return range;
}

string stripStart(string str, string strip)
{
	if (str.startsWith(strip))
		return str[strip.length .. $];
	else
		return str;
}

string ellipsis(string text, int length)
in(length >= 3)
{
	if (text.length > length)
		return text[0 .. length - 3] ~ "...";
	else
		return text;
}

struct Project
{
	string name;
	string web_url;
}

struct GitlabPushHook
{
	struct Commit
	{
		struct Author
		{
			string name;
			string email;
		}

		string id;
		string url;
		string message;
		Author author;
	}

	string ref_;
	string after;
	string before;
	@optional string user_name;
	@optional string user_username;
	string user_avatar;
	int total_commits_count;
	Project project;
	Commit[] commits;
}

struct GitlabTagPushHook
{
	string after;
	string before;
	string ref_;
	@optional string user_name;
	@optional string user_username;
	string user_avatar;
	Project project;
}

struct GitlabIssueHook
{
	struct User
	{
		string name;
		string username;
		string avatar_url;
	}

	struct Attributes
	{
		@optional int iid = -1;
		@optional string title;
		@optional string description;
		@optional string url;
		@optional string action;
	}

	User user;
	Project project;
	@optional Attributes object_attributes;
}

struct GitlabMergeRequestHook
{
	struct User
	{
		string name;
		string username;
		string avatar_url;
	}

	struct Attributes
	{
		@optional int iid = -1;
		@optional string title;
		@optional string description;
		@optional string url;
		@optional string action;
	}

	User user;
	Project project;
	@optional Attributes object_attributes;
}

mixin template GitlabNoteHook()
{
	struct User
	{
		string name;
		string username;
		string avatar_url;
	}

	struct Attributes
	{
		struct Diff
		{
			string diff;
		}

		@optional int id = -1;
		@optional string note;
		@optional string url;
		@optional Diff st_diff;
	}

	User user;
	Project project;
	@optional Attributes object_attributes;
}

struct GitlabCommitNoteHook
{
	mixin GitlabNoteHook;

	struct Commit
	{
		string id;
	}

	Commit commit;
}

struct GitlabMergeRequestNoteHook
{
	mixin GitlabNoteHook;

	struct MergeRequest
	{
		int iid;
		string title;
	}

	MergeRequest merge_request;
}

struct GitlabIssueNoteHook
{
	mixin GitlabNoteHook;

	struct Issue
	{
		int iid;
		string title;
	}

	Issue issue;
}

struct GitlabSnippetNoteHook
{
	mixin GitlabNoteHook;

	struct Snippet
	{
		string title;
	}

	Snippet snippet;
}

struct GitlabWikiHook
{
	struct User
	{
		string name;
		string username;
		string avatar_url;
	}

	struct Attributes
	{
		@optional string title;
		@optional string content;
		@optional string url;
		@optional string action;
	}

	User user;
	Project project;
	@optional Attributes object_attributes;
}

struct GitlabPipelineHook
{
	struct User
	{
		string name;
		string username;
		string avatar_url;
	}

	struct Attributes
	{
		@optional int id = -1;
		@optional string status;
		@optional int duration;
	}

	User user;
	Project project;
	@optional Attributes object_attributes;
}

struct Embed
{
	struct Author
	{
		string name;
		string url;
		string icon_url;
	}

	string title;
	string url;
	string description;
	Author author;
	int color = 0xcacbce;
}

string postDiscord(string url, Embed embed)
{
	int status;
	string message;
	embed.description = embed.description.ellipsis(500);
	requestHTTP(url, (scope req) {
		req.headers["Content-Type"] = "application/json";
		req.headers["User-Agent"] = "GitlabDiscordHook/webfreak.org";
		req.method = HTTPMethod.POST;
		req.writeJsonBody(["embeds" : [embed]]);
		logInfo("%s", serializeToJson(embed));
	}, (scope res) {
		status = res.statusCode;
		if (status >= 400)
			message = res.bodyReader.readAllUTF8;
	});
	return status < 400 ? "success" : "remote-fail (" ~ status.to!string ~ ") " ~ message;
}

string sendTranslated(string event, string json, string url)
{
	switch (event)
	{
	case "Push Hook":
		return sendPush(deserializeJson!GitlabPushHook(parseJsonString(json)), url);
	case "Tag Push Hook":
		return sendTagPush(deserializeJson!GitlabTagPushHook(parseJsonString(json)), url);
	case "Issue Hook":
		return sendIssue(deserializeJson!GitlabIssueHook(parseJsonString(json)), url);
	case "Note Hook":
		auto obj = parseJsonString(json);
		string type = obj["object_attributes"]["noteable_type"].get!string;
		switch (type)
		{
		case "Commit":
			return sendCommitNote(deserializeJson!GitlabCommitNoteHook(obj), url);
		case "MergeRequest":
			return sendMergeRequestNote(deserializeJson!GitlabMergeRequestNoteHook(obj), url);
		case "Issue":
			return sendIssueNote(deserializeJson!GitlabIssueNoteHook(obj), url);
		case "Snippet":
			return sendSnippetNote(deserializeJson!GitlabSnippetNoteHook(obj), url);
		default:
			return "not implemented";
		}
	case "Merge Request Hook":
		return sendMergeRequest(deserializeJson!GitlabMergeRequestHook(parseJsonString(json)), url);
	case "Wiki Page Hook":
		return sendWiki(deserializeJson!GitlabWikiHook(parseJsonString(json)), url);
	case "Pipeline Hook":
		return sendPipeline(deserializeJson!GitlabPipelineHook(parseJsonString(json)), url);
	case "Build Hook":
		return "not implemented";
	default:
		return "Unknown X-Gitlab-Event header";
	}
}

string sendPush(GitlabPushHook push, string url)
{
	if (!push.project.name.length)
		return "no project name";
	if (!push.total_commits_count)
		return "no commits";

	string title = format!"[%s] %d new commit%s"(push.project.name,
			push.total_commits_count, push.total_commits_count == 1 ? "" : "s");
	string compareUrl = format!"%s/compare/%s...%s"(push.project.web_url,
			push.before.shorten(7), push.after.shorten(7));
	string description;
	foreach (commit; push.commits.shorten(5))
		description ~= format!"[`%s`](%s) %s%s\n"(commit.id.shorten(7), commit.url,
				ellipsis(commit.message.strip, 50), commit.author.name.strip.length
				? " - " ~ commit.author.name.strip : "");
	string authorName = push.user_username.length ? push.user_username : push.user_name;
	URL authorUrl = URL(push.project.web_url);
	authorUrl.pathString = "/" ~ push.user_username.encodeComponent;
	string authorPicture = push.user_avatar;
	return postDiscord(url, Embed(title, compareUrl, description,
			Embed.Author(authorName, authorUrl.toString, authorPicture), 0x7289da));
}

string sendTagPush(GitlabTagPushHook push, string url)
{
	if (!push.project.name.length)
		return "no project name";

	string title = format!"[%s] new tag authored: %s"(push.project.name,
			push.ref_.stripStart("refs/tags/"));
	string compareUrl = format!"%s/compare/%s...%s"(push.project.web_url,
			push.before.shorten(7), push.after.shorten(7));
	string authorName = push.user_username.length ? push.user_username : push.user_name;
	URL authorUrl = URL(push.project.web_url);
	authorUrl.pathString = "/" ~ push.user_username.encodeComponent;
	string authorPicture = push.user_avatar;
	return postDiscord(url, Embed(title, compareUrl, null,
			Embed.Author(authorName, authorUrl.toString, authorPicture)));
}

string sendIssue(GitlabIssueHook issue, string url)
{
	if (!issue.project.name.length)
		return "no project name";
	if (issue.object_attributes.action == "open" || issue.object_attributes.action == "reopen")
	{
		string title = format!"[%s] Issue %sed: #%d %s"(issue.project.name,
				issue.object_attributes.action, issue.object_attributes.iid,
				issue.object_attributes.title);
		string description = issue.object_attributes.description;
		string authorName = issue.user.username.length ? issue.user.username : issue.user.name;
		URL authorUrl = URL(issue.project.web_url);
		authorUrl.pathString = "/" ~ issue.user.username.encodeComponent;
		string authorPicture = issue.user.avatar_url;
		return postDiscord(url, Embed(title, issue.object_attributes.url, description,
				Embed.Author(authorName, authorUrl.toString, authorPicture), 0xeb6420));
	}
	else if (issue.object_attributes.action == "close")
	{
		string title = format!"[%s] Issue closed: #%d %s"(issue.project.name,
				issue.object_attributes.iid, issue.object_attributes.title);
		string authorName = issue.user.username.length ? issue.user.username : issue.user.name;
		URL authorUrl = URL(issue.project.web_url);
		authorUrl.pathString = "/" ~ issue.user.username.encodeComponent;
		string authorPicture = issue.user.avatar_url;
		return postDiscord(url, Embed(title, issue.object_attributes.url, null,
				Embed.Author(authorName, authorUrl.toString, authorPicture)));
	}
	else
	{
		return "unsupported action " ~ issue.object_attributes.action;
	}
}

string sendMergeRequest(GitlabMergeRequestHook mr, string url)
{
	if (!mr.project.name.length)
		return "no project name";
	if (mr.object_attributes.action == "open" || mr.object_attributes.action == "reopen")
	{
		string title = format!"[%s] Merge request %sed: #%d %s"(mr.project.name,
				mr.object_attributes.action, mr.object_attributes.iid, mr.object_attributes.title);
		string description = mr.object_attributes.description;
		string authorName = mr.user.username.length ? mr.user.username : mr.user.name;
		URL authorUrl = URL(mr.project.web_url);
		authorUrl.pathString = "/" ~ mr.user.username.encodeComponent;
		string authorPicture = mr.user.avatar_url;
		return postDiscord(url, Embed(title, mr.object_attributes.url, description,
				Embed.Author(authorName, authorUrl.toString, authorPicture), 0x009800));
	}
	else if (mr.object_attributes.action == "close")
	{
		string title = format!"[%s] Merge request closed: #%d %s"(mr.project.name,
				mr.object_attributes.iid, mr.object_attributes.title);
		string authorName = mr.user.username.length ? mr.user.username : mr.user.name;
		URL authorUrl = URL(mr.project.web_url);
		authorUrl.pathString = "/" ~ mr.user.username.encodeComponent;
		string authorPicture = mr.user.avatar_url;
		return postDiscord(url, Embed(title, mr.object_attributes.url, null,
				Embed.Author(authorName, authorUrl.toString, authorPicture)));
	}
	else
	{
		return "unsupported action " ~ mr.object_attributes.action;
	}
}

string sendCommitNote(GitlabCommitNoteHook note, string url)
{
	if (!note.project.name.length)
		return "no project name";
	string title = format!"[%s] Comment on commit %s"(note.project.name, note.commit.id.shorten(7));
	string authorName = note.user.username.length ? note.user.username : note.user.name;
	string description;
	if (note.object_attributes.st_diff.diff.length)
		description ~= format!"```diff\n%s\n```\n\n"(note.object_attributes.st_diff.diff);
	description ~= note.object_attributes.note;
	URL authorUrl = URL(note.project.web_url);
	authorUrl.pathString = "/" ~ note.user.username.encodeComponent;
	string authorPicture = note.user.avatar_url;
	return postDiscord(url, Embed(title, note.object_attributes.url, description,
			Embed.Author(authorName, authorUrl.toString, authorPicture)));
}

string sendMergeRequestNote(GitlabMergeRequestNoteHook note, string url)
{
	if (!note.project.name.length)
		return "no project name";
	string title = format!"[%s] Comment on merge request #%d %s"(note.project.name,
			note.merge_request.iid, note.merge_request.title);
	string authorName = note.user.username.length ? note.user.username : note.user.name;
	string description = note.object_attributes.note;
	URL authorUrl = URL(note.project.web_url);
	authorUrl.pathString = "/" ~ note.user.username.encodeComponent;
	string authorPicture = note.user.avatar_url;
	return postDiscord(url, Embed(title, note.object_attributes.url, description,
			Embed.Author(authorName, authorUrl.toString, authorPicture)));
}

string sendIssueNote(GitlabIssueNoteHook note, string url)
{
	if (!note.project.name.length)
		return "no project name";
	string title = format!"[%s] Comment on issue #%d %s"(note.project.name,
			note.issue.iid, note.issue.title);
	string authorName = note.user.username.length ? note.user.username : note.user.name;
	string description = note.object_attributes.note;
	URL authorUrl = URL(note.project.web_url);
	authorUrl.pathString = "/" ~ note.user.username.encodeComponent;
	string authorPicture = note.user.avatar_url;
	return postDiscord(url, Embed(title, note.object_attributes.url, description,
			Embed.Author(authorName, authorUrl.toString, authorPicture)));
}

string sendSnippetNote(GitlabSnippetNoteHook note, string url)
{
	if (!note.project.name.length)
		return "no project name";
	string title = format!"[%s] Comment on snippet %s"(note.project.name, note.snippet.title);
	string authorName = note.user.username.length ? note.user.username : note.user.name;
	string description = note.object_attributes.note;
	URL authorUrl = URL(note.project.web_url);
	authorUrl.pathString = "/" ~ note.user.username.encodeComponent;
	string authorPicture = note.user.avatar_url;
	return postDiscord(url, Embed(title, note.object_attributes.url, description,
			Embed.Author(authorName, authorUrl.toString, authorPicture)));
}

string sendWiki(GitlabWikiHook wiki, string url)
{
	if (!wiki.project.name.length)
		return "no project name";
	string title = format!"[%s] Wiki %s: %s"(wiki.project.name,
			wiki.object_attributes.action, wiki.object_attributes.title);
	string description = wiki.object_attributes.content;
	string authorName = wiki.user.username.length ? wiki.user.username : wiki.user.name;
	URL authorUrl = URL(wiki.project.web_url);
	authorUrl.pathString = "/" ~ wiki.user.username.encodeComponent;
	string authorPicture = wiki.user.avatar_url;
	return postDiscord(url, Embed(title, wiki.object_attributes.url, description,
			Embed.Author(authorName, authorUrl.toString, authorPicture)));
}

string sendPipeline(GitlabPipelineHook pipeline, string url)
{
	if (!pipeline.project.name.length)
		return "no project name";
	string title = format!"[%s] pipeline build #%d %s after %s"(pipeline.project.name, pipeline.object_attributes.id,
			pipeline.object_attributes.status, pipeline.object_attributes.duration.seconds);
	string authorName = pipeline.user.username.length ? pipeline.user.username : pipeline.user.name;
	URL authorUrl = URL(pipeline.project.web_url);
	authorUrl.pathString = "/" ~ pipeline.user.username.encodeComponent;
	string authorPicture = pipeline.user.avatar_url;
	return postDiscord(url, Embed(title, pipeline.project.web_url, null,
			Embed.Author(authorName, authorUrl.toString, authorPicture),
			pipeline.object_attributes.status == "success" ? 0x5fe35f : 0xfc5c3c));
}
